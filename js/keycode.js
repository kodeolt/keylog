window.onload = () => {
    // Config
    // Change method and url
    // const httpRequest = new XMLHttpRequest();
    // let req = {
    //     _method: 'POST',
    //     _baseUrl: 'data.php'
    // };

    const inputElements = document.querySelectorAll('input:not([name=username]), textarea');

    let pressedKeysArray = [];
    let username;

    // Set username on input change
    let usernameInput = document.querySelector('input[name=username]');

    // If usernameInput doesnt exist in DOM, dont set `username`
    if (usernameInput) {
        if(localStorage.getItem('usernameInputVal')) {
            username = localStorage.getItem('usernameInputVal');
        } else {
            usernameInput.addEventListener('blur', (event) => {
                localStorage.setItem('usernameInputVal', usernameInput.value);
                username = usernameInput.value;
            });
        }
    } else {
        if (localStorage.getItem('usernameID')) {
            username = localStorage.getItem('usernameID');
        }
    }

    inputElements.forEach((elem) => {
        const uuid = generateUuid();
        // Key was pressed
        elem.addEventListener('keydown', (event) => {
            // Set data
            let key = {
                _keyCode: event.keyCode,
                _name: event.key,
                _action: 'keydown',

                _inputUuid: uuid,
                _username: username || '',

                _timestamp: Date.now(),
                _location: event.location,

                // If needed
                _altKey: event.altKey,
                _ctrlKey: event.ctrlKey,
                _shiftKey: event.shiftKey
            };

            // Send data
            sendData(key);

            // console.log(key);

            if (initObj) {
                initObj.hasUserStartedTyping = true;
            }

            // Add to array
            pressedKeysArray.push(key);
        });

        // Key was released
        elem.addEventListener('keyup', (event) => {
            pressedKeysArray.forEach((key) => {
                if (key._keyCode === event.keyCode && key._action === 'keydown') {
                    key._action = 'keyup';
                    key._timestamp = Date.now();

                    // console.log(key);

                    sendData(key);
                    return;
                }
            });
        });
    });

    // Finished, send to write to file
    var button = document.getElementById("mygtukas");

    // If button doesnt exist in DOM, do nothing
    if (button && button.length > 0) {
        button.addEventListener('click', (event) => {
            var request = new XMLHttpRequest();
            request.open('GET', '/Home/Finished');
            request.send();
        });
    }

    // Generate UUID
    function generateUuid() {
        let current = new Date().getTime();

        let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (u) => {
            let r = (current + Math.random() * 16) % 16 | 0;
            current = Math.floor(current / 16);

            return (u === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });

        return uuid;
    }

    // Send data somewhere
    function sendData(data = {}) {
        // console.log(data);
        httpRequest = new XMLHttpRequest();
        httpRequest.open('POST', 'data.php', true);
        httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        httpRequest.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                //  Do something with successfull response
                let rs = this.responseText;
                if (rs) {
                    if (rs === "True") {
                        // console.log('Praise the lord. TRUE TO ALMIGHTY');
                        initObj.isUserAuthenticated = true;
                        initObj.responseTrueCounter++;
                    } else if(rs === "False") {
                        // console.log('Nigga, get out of my birthday. ITS FALSE');
                        initObj.isUserAuthenticated = false;
                        initObj.responseFalseCounter++;
                    }
                } else {
                    // console.log('another time asshole. NULL COMES TO THE PARTY');
                }
            } else {
                //  Do something with failed response
                console.log(this.code)
                console.log('failed response');
            }
        }
        httpRequest.send('data=' + JSON.stringify(data));
    }

    // **************************************************************
    // Plugin part, where you can click on input, and opens a modal
    // to write text and track changes, if correct user is writing.
    // TODO: Refactor, rename some stuff,
    // TODO: make everything as one big object?
    // **************************************************************

    /*
        Clicking on input
     */
    class MainObject {

        constructor(className, options = []) {

            this.isUserAuthenticated = false;
            this.hasUserStartedTyping = false;

            // Main class name which will be used everywhere
            this.className = className;

            // All elements with has main class name
            this.elems = this.getAllInputElementsByClassName();

            // Modal window class name and element
            this.modalClassName = `${this.className}__popup`;
            this.modal = this.getModalElement();

            this.headingClassName = `${this.modalClassName} .heading label`;
            this.modalQuestion = '';

            this.question = null;
            this.questionClassName = `${this.className}__question`;

            this.editor = this.getEditorTextarea();

            this.focusedElement = null;

            // Save button DOM element
            this.saveButton = this.getSaveButton();

            this.responseTrueCounter = 0;
            this.responseFalseCounter = 0;

            // Init methods
            this.onFocusElement();
            this.outOfFocusElement();
            this.saveButtonListener();

            if (this.modal) {
                this.modal.querySelector('.heading .close').onclick = (e) => {
                    this.saveAnswer();
                }
            }
        }

        getAllInputElementsByClassName() {
            return document.querySelectorAll(`.${this.className}`) || null;
        }

        getModalElement() {
            return document.querySelector(`.${this.className}__popup`) || null
        }

        getEditorTextarea() {
            if (!this.modal) {
                return null;
            }

            return this.modal.querySelector(`.${this.modalClassName} textarea`) || null;
        }

        // Get closest div question by
        getQuestion() {
            if (!this.focusedElement) {
                return null;
            }

            return this.focusedElement.parentNode.querySelector(`.${this.questionClassName}`).textContent;
        }

        setModalQuestion() {
            this.modal.querySelector(`.${this.headingClassName}`).textContent = this.question;
        }

        getSaveButton() {
            if (!this.modal) {
                return null;
            }

            return this.modal.querySelector(`.${this.modalClassName} .button--save`) || null;
        }

        // Get all textarea elements and listen for `focus` event,
        // if focus on elem, open modal where user types
        onFocusElement() {
            if (!this.elems)
                return;

            this.elems.forEach((elem) => {
                elem.onfocus = () => {
                    if (!this.focusedElement) {
                        this.focusedElement = elem;

                        this.question = this.getQuestion();

                        this.setModalQuestion();

                        let val = this.focusedElement.value;
                        if (val) {
                            if(val.search(`\n\n--------------------\nID - ${localStorage.getItem('usernameID')}`) !== -1) {
                                val = val.replace(`\n\n--------------------\nID - ${localStorage.getItem('usernameID')}`, '');
                            } else {
                                val = val.replace(`\n\n--------------------\nID - undefined`, '');
                            }
                            this.editor.value = val;
                        }
                    }
                    this.modal.classList.add('modal--show');

                    setTimeout(() => {
                        this.editor.select();
                    }, 150)
                }
            });
        }



        /**
         * Hide modal if it's already showing and clicking outside div
         */
        outOfFocusElement() {
            if (!this.modal)
                return;

            this.modal.onclick = (e) => {
                if (e.target.className.search('modal--show') !== -1) {
                    this.saveAnswer();
                }
            }
        }

        checkIfUserIsAuthenticated() {
            if (!this.isUserAuthenticated) {
                return false;
            }

            return true;
        }

        hideModal() {
            this.modal.classList.remove('modal--show');
        }

        saveButtonListener() {
            if (!this.saveButton) {
                return;
            }

            this.saveButton.onclick = (e) => {
                this.saveAnswer();
            }
        }

        saveAnswer() {
            if (this.focusedElement) {
                if (this.hasUserStartedTyping) {
                    let answer = this.editor.value;

                    // Reset editor value
                    this.editor.value = null;

                    // Save value to parent input
                    this.focusedElement.value = answer;

                    if(this.responseTrueCounter > this.responseFalseCounter) {
                        this.focusedElement.value += "\n\n";
                        this.focusedElement.value += "--------------------\n";
                        this.focusedElement.value += `ID - ${localStorage.getItem('usernameID')}`;
                    } else {
                        this.focusedElement.value += "\n\n";
                        this.focusedElement.value += "--------------------\n";
                        this.focusedElement.value += `ID - undefined`;
                    }

                    this.responseTrueCounter = 0;
                    this.responseFalseCounter = 0;
                }

                this.focusedElement = null;
                this.hasUserStartedTyping = false;

                this.question = '';

                this.hideModal();
            }
        }
        // .end of class
    }

    // Obj options
    let options = [

    ];

    // Init object
    let initObj = new MainObject('definitelyNotKeylogger');
    console.log(initObj);


    const buttonLogout = document.querySelector('.btn--logout');
    if (buttonLogout && buttonLogout.length > 0) {
        buttonLogout.onclick = (e) => {
            e.preventDefault();

            localStorage.clear();
            window.location.href = "login.html"
        }
    }

}