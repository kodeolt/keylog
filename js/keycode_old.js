window.onload = () => {

    // Config
    // Change method and url
    const httpRequest = new XMLHttpRequest();
    let req = {
        _method: 'POST',
        _baseUrl: 'data.php'
    };

    const inputElements = document.querySelectorAll('input:not([name=username]), textarea');

    let pressedKeysArray = [];
    let username;

    // Set username on input change
    let usernameInput = document.querySelector('input[name=username]');

    usernameInput.addEventListener('blur', () => {
        username = usernameInput.value
    });

    /*
     * Loop through all inputable elements.
     * On each element listen for keydown/keyup events
     */
    inputElements.forEach((elem) => {
        const uuid = generateUuid();

        // Key was pressed
        elem.onkeydown = (e) => {
            console.log(e);
            if(pressedKeysArray[e.keyCode] && !pressedKeysArray[e.keyCode]._releasedAt)
                return;

            // Set data
            let key = {
                _keyCode: e.keyCode,
                _name: e.key,
                _action: 'keydown',

                _inputUuid: uuid,
                _username: username || '',

                _timestamp: Date.now(),
                _location: e.location,

                // If needed
                _altKey: e.altKey,
                _ctrlKey: e.ctrlKey,
                _shiftKey: e.shiftKey
            }

            // Send data
            sendData(key);

            // Add to array
            pressedKeysArray[e.keyCode] = key;
        };

        // Key was released
        elem.onkeyup = (e) => {
            if(!pressedKeysArray[e.keyCode])
                return;

            pressedKeysArray[e.keyCode]._action = 'keyup';
            pressedKeysArray[e.keyCode]._timestamp = Date.now();

            // Send data
            sendData(pressedKeysArray[e.keyCode]);

            // Just for logging stuff
            printToPage(pressedKeysArray[e.keyCode]);
        };
    });

    function printToPage(key) {

        let pressedKeysContainer = document.getElementById('pressedKeys_list');
        let listItem = document.createElement('li');

        listItem.textContent = `Key '${key._name}' was pressed. Input UUID - ${key._inputUuid}. Location: ${key._location}`;
        pressedKeysContainer.prepend(listItem);
    }

    // Generate UUID
    function generateUuid() {
        let current = new Date().getTime();

        let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (u) => {
            let r = (current + Math.random() * 16) % 16 | 0;
            current = Math.floor(current / 16);

            return (u === 'x' ? r : (r&0x3|0x8)).toString(16);
        });

        return uuid;
    }

    httpRequest.onload = () => {
      if(httpRequest.status >= 200 && httpRequest.status < 300) {
            //  Do something with successfull response
          console.log(httpRequest.responseText);
      } else {
          //  Do something with failed response
          console.log('failed');
      }
    };

    // Send data somewhere
    function sendData(data = {}) {
        httpRequest.open(req._method, req._baseUrl, true);
        httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        httpRequest.send('data=' + JSON.stringify(data));
    }
}